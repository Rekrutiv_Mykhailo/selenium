package com.epam;

import com.epam.actions.LogInActions;
import com.epam.driver.DriverInitializator;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MailTest {
    private LogInActions logInActions;
    @BeforeTest
    public void init(){
        logInActions = new LogInActions(DriverInitializator.getDriver("chrome"),10);
    }
    @Test
    public void testMailDeleting(){
        logInActions.logIn("tttest.mmail@gmail.com","Green1234!@");
        logInActions.deleteFirstThreeMessages();
    }
}
