package com.epam.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.HashMap;
import java.util.Map;

public class DriverInitializator {
    private static Map<String, WebDriver> DRIVERS = new HashMap<>();

    static {
        DRIVERS.put("chrome", null);
        DRIVERS.put("firefox", null);
    }

    public static WebDriver getDriver(String nameBrowser) {
        WebDriver webDriver;
        if ("chrome".equals(nameBrowser)) {
            System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
            if (DRIVERS.get(nameBrowser) == null) {
                DRIVERS.replace(nameBrowser, new ChromeDriver());
            }
            webDriver = DRIVERS.get(nameBrowser);
        } else if ("firefox".equals(nameBrowser)) {
            webDriver = new FirefoxDriver();
        } else {
            webDriver = new ChromeDriver();
        }
        return webDriver;
    }
    public static void quitDriver(String browserName){
        DRIVERS.get(browserName).quit();
        DRIVERS.put(browserName,null);
    }
}
