package com.epam.actions;

import com.epam.pages.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LogInActions {
    private LoginPage loginPage;

    public LogInActions(WebDriver webDriver, int wait) {
        loginPage = new LoginPage(webDriver);
    }

    public void logIn(String mail, String password) {
        typeMail(mail);
        loginPage.getButtonMailNext().click();
        typePassword(password);
        loginPage.getButtonPasswordNext().click();
        loginPage.getUserIcon().click();
    }

    private void checkFirstThreeMessages() {
        loginPage.getFirstMessage().click();
        loginPage.getSecondMessage().click();
        loginPage.getThirdMessage().click();
    }

    public void deleteFirstThreeMessages() {
        checkFirstThreeMessages();
        loginPage.getDeleteMessagesButton().click();
    }

    private void typeMail(String mail) {
        loginPage.getEmailInput().sendKeys(mail);
    }

    private void typePassword(String password) {
        loginPage.getPasswordInput().sendKeys(password);
    }

    public boolean assertLogIn(String mail) {
        return loginPage.getUserIcon().getAttribute("aria-label").contains(mail);
    }
}
